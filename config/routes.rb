TravexV2::Application.routes.draw do

  Page.all.each do |page|
    get "#{page.anchor}" => 'application#page'
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)


  get '/:country/hotels' => 'countries#hotels', as: 'hotels'
  get '/:country/visa' => 'countries#visa', as: 'visa'
  get '/:country/request' => 'countries#request_tour', as: 'request_tour'

  get '/:country' => 'countries#show', as: 'country'
  get '/:country/:city' => 'cities#show', as: 'city'
  get '/:country/:city/:hotel' => 'hotels#show', as: 'hotel'

end