<?php

header('Access-Control-Allow-Origin: *');

$hotel = $_POST['hotel'];

$payment = array('Наличный расчёт', 'Безналичный расчет', 'Кредитная карта');
$types = array('Одноместный', 'Двуместный');

$byu = $payment[(int)$hotel['byu']];
$city = $hotel['city'];
$comment = $hotel['comment'];
$count = $hotel['count'];
$country = $hotel['country'];
$email = $hotel['email'];
$fax = $hotel['fax'];
$hotel_name = $hotel['hotel'];
$in_date = $hotel['in_date'];
$contact = $hotel['contact'];
$out_date = $hotel['out_date'];
$phone = $hotel['phone'];
$price = $hotel['price'];
$type = $types[(int)$hotel['type']];

$message = "
  <strong>Имя:</strong> $contact <br />
  <strong>Email:</strong> $email <br />
  <strong>Факс:</strong> $fax <br />
  <strong>Телефон:</strong> $phone <br /><br />
  <strong>Страна:</strong> $country <br />
  <strong>Город:</strong> $city <br />
  <strong>Отель:</strong> $hotel_name <br /><br />
  <strong>Способ оплаты:</strong> $byu <br />
  <strong>Количество номеров:</strong> $count <br />
  <strong>Дата заезда:</strong> $in_date <br />
  <strong>Дата отъезда:</strong> $out_date <br />
  <strong>Тип номера:</strong> $type <br />
  <strong>Ориентировочная цена:</strong> $price <br />
  <strong>Комментарий:</strong> $comment <br />

";




$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

echo 'ok';

mail('hotel@travex.ru', 'Заказ с Active Travel.', $message, $headers);

?>