<?php

header('Access-Control-Allow-Origin: *');

$train = $_POST['train'];

$out_time = array('Утро', 'День', 'Вечер');
$class_type = array('Экономичный', 'Бизнес', 'Первый');
$payment = array('Наличный расчёт', 'Безналичный расчет', 'Кредитная карта');

$class_type = $class_type[(int)$train['class_type']];
$email = $train['email'];
$in_date = $train['in_date'];
$in_point = $train['in_point'];
$name = $train['name'];
$note = $train['note'];
$out_date = $train['out_date'];
$out_point = $train['out_point'];
$out_time = $out_time[(int)$train['out_time']];
$payment = $payment[(int)$train['payment']];
$phone = $train['phone'];
$route = $train['route'];


$message = "
  <strong>Имя:</strong> $name <br />
  <strong>Телефон:</strong> $phone <br />
  <strong>Email:</strong> $email<br />
  <br />
  <strong>Дата выезда:</strong> $out_date<br />
  <strong>Дата приезда:</strong> $in_date<br />
  <br />
  <strong>Пункт выезда:</strong> $out_point<br />
  <strong>Пункт приезда:</strong> $in_point<br />
  <strong>Маршрут:</strong> $route<br />
  <strong>Примечания:</strong> $note<br />
  <br />
  <strong>Время выезда:</strong> $out_time<br />
  <strong>Класс:</strong> $class_type<br />
  <strong>Форма оплаты:</strong> $payment<br />
  <br />
";

foreach ($train['passager'] as $key => $value) {
  $fio = $value['fio'];
  $birth = $value['birth'];
  $message .= "
    <strong>Пассажир $key:</strong><br />
    &nbsp;&nbsp;<strong>ФИО:</strong> $fio<br />
    &nbsp;&nbsp;<strong>Дата рождения:</strong> $birth<br /><br />
  ";
}

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/plain; charset=utf-8' . "\r\n";

echo 'ok';

mail('karpenkova@travex.ru', 'Заказ с Active Travel.', $message, $headers);

/*
class_type
email
in_date
in_point
name
note
out_date
out_point
out_time
payment
phone
route
*/

?>