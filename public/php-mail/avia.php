<?php

header('Access-Control-Allow-Origin: *');

$avia = $_POST['avia'];

$out_time = array('Утро', 'День', 'Вечер');
$class_type = array('Экономичный', 'Бизнес', 'Первый');
$payment = array('Наличный расчёт', 'Безналичный расчет', 'Кредитная карта');
$years_a = array('"Взрослый"(>25 лет)', '"Молодежный"(12-25 лет)', '"Ребенок"(2-12 лет)', '"Младенец"(0-2 лет)');

$name = $avia['name'];
$phone = $avia['phone'];
$fax = $avia['fax'];
$email = $avia['email'];
$out_date = $avia['out_date'];
$in_date = $avia['in_date'];
$out_point = $avia['out_point'];
$in_point = $avia['in_point'];
$route = $avia['route'];
$company = $avia['company'];
$note = $avia['note'];
$out_time = $out_time[(int)$avia['out_time']];
$class_type = $class_type[(int)$avia['class_type']];
$payment = $payment[(int)$avia['payment']];

$message = "
  <strong>Имя:</strong> $name <br />
  <strong>Телефон:</strong> $phone <br />
  <strong>Факс:</strong> $fax<br />
  <strong>Email:</strong> $email<br />
  <br />
  <strong>Дата вылета:</strong> $out_date<br />
  <strong>Дата прилёта:</strong> $in_date<br />
  <br />
  <strong>Пункт вылета:</strong> $out_point<br />
  <strong>Пункт прилёта:</strong> $in_point<br />
  <strong>Маршрут:</strong> $route<br />
  <strong>Авиакомпания:</strong> $company<br />
  <strong>Примечания:</strong> $note<br />
  <br />
  <strong>Время вылета:</strong> $out_time<br />
  <strong>Класс:</strong> $class_type<br />
  <strong>Форма оплаты:</strong> $payment<br />
  <br />
";



foreach ($avia['passager'] as $key => $value) {
  $fio = $value['fio'];
  $years = $years_a[(int)$value['years']];
  $message .= "
    <strong>Пассажир $key:</strong><br />
    &nbsp;&nbsp;<strong>ФИО:</strong> $fio<br />
    &nbsp;&nbsp;<strong>Возраст:</strong> $years<br /><br />
  ";
}


$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

echo 'ok';

mail('avia@travex.ru', 'Заказ с Active Travel.', $message, $headers);

?>