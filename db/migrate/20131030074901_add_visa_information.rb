class AddVisaInformation < ActiveRecord::Migration
  def up
    add_column :countries, :visa_information, :text
  end

  def down
  end
end