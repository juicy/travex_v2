class AddAnchores < ActiveRecord::Migration
  def up
    add_column :countries, :anchor, :string
    add_column :cities, :anchor, :string
    add_column :hotels, :anchor, :string
  end

  def down
  end
end