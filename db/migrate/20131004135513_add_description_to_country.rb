class AddDescriptionToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :description, :text
    add_attachment :countries, :image
  end
end