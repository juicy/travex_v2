class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :anchor
      t.string :title
      t.text :content
      t.string :header, default: 'index'
      t.string :header4, default: 'false'
      
      t.timestamps
    end
  end
end
