class AddSortToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :sort, :integer
  end
end