class Page < ActiveRecord::Base
  attr_accessible :anchor, :content, :title, :header, :header4

  def train?
    return true if self.anchor == '/train'
    false
  end

  def visas?
    return true if self.anchor == '/visas'
    false
  end

  def avia?
    return true if self.anchor == '/avia'
    false
  end

  def root?
    return true if self.anchor == '/'
    false
  end

end