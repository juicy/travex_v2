class Hotel < ActiveRecord::Base
  attr_accessible :city_id, :title, :anchor

  belongs_to :city, :class_name => "City", :foreign_key => "city_id"

end
