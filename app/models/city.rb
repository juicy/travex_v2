class City < ActiveRecord::Base
  attr_accessible :country_id, :title, :anchor

  belongs_to :country, :class_name => "Country", :foreign_key => "country_id"
  has_many :hotels, :class_name => "Hotel", :foreign_key => "city_id"

end
