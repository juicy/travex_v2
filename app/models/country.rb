class Country < ActiveRecord::Base
  attr_accessible :title, :anchor, :description, :image, :visa_information
  has_attached_file :image,
                    :storage => :dropbox,
                    :dropbox_credentials => Rails.root.join("#{Rails.root}/config/dropbox.yml"),
                    :dropbox_options => {:path => proc { |style| "countries/#{id}/#{style}/#{image.original_filename}" }}

  has_many :cities, :class_name => "City", :foreign_key => "country_id"
  has_many :hotels, :through => :cities

end
