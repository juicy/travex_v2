ActiveAdmin.register Page do

  index do
    selectable_column
    column :title
    column :anchor
    actions :defaults => true
  end

end