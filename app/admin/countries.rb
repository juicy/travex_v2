ActiveAdmin.register Country do

  index do
    selectable_column
    column :title
    column :anchor
    actions :defaults => true
  end

  form :html => { :enctype => "multipart/form-data" } do |f|  
    f.inputs do
      f.input :title
      f.input :anchor
      f.input :description
      f.input :visa_information
      f.input :image, :as => :file
    end
    f.buttons
  end

end
