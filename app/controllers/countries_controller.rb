class CountriesController < ApplicationController

  before_filter :find

  def show
  end

  def visa
  end

  def hotels
  end

  def request_tour
  end

private

  def find
    @country = Country.find_by_anchor(params[:country])
  end

end
