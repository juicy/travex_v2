class ApplicationController < ActionController::Base
  protect_from_forgery

  protect_from_forgery except: :page
  before_filter :boxs

  def page
    @page = Page.find_by_anchor(request.fullpath)
  end

private

  def boxs
    @boxs = Box.all
  end

end
