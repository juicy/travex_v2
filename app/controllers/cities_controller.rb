class CitiesController < ApplicationController

  def show
    @city = City.find_by_anchor(params[:city])
    @country = @city.country
  end

end
