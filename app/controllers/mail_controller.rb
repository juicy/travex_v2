class MailController < ApplicationController

  def avia
    Mails.avia(params[:avia]).deliver
    render text: true
  end

  def hotel
    Mails.hotel(params[:hotel]).deliver
    render text: true
  end

end
