class HotelsController < ApplicationController

  def show
    @hotel = Hotel.find_by_anchor(params[:hotel])
    @city = @hotel.city
    @country = @city.country
  end

end
