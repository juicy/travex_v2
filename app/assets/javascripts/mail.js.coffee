# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready () ->
  $('form.form').validate({
    errorElement: "hide",
    submitHandler: (form) ->
      $.post(
        $(form).attr('action'),
        $(form).serialize(),
        (data) ->
          if data == 'ok'
            alert 'Ваше заявка отправлена! Наши менеджеры свяжутся с вами'
            $(form)[0].reset()
          else
            alert data
      )
  });