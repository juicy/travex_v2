$(document).ready ->
  avia_form('form#avia')
  train_form('form#train')
  $('input.date_i').pickadate()

avia_form = (box) ->
  i = 1
  $(box).find('input.add_passager').click ->
    code = '<tr><td class="fio"><input type="text" name="avia[passager][' + i + '][fio]" required></td><td><select name="avia[passager][' + i + '][years]"><option value="0">"Взрослый"(&gt;25 лет)</option><option value="1">"Молодежный"(12-25 лет)</option><option value="2">"Ребенок"(2-12 лет)</option><option value="3">"Младенец"(0-2 лет)</option></select></td></tr>'
    $(code).insertBefore $(box).find('tr.after_pass')
    i++
    false

train_form = (box) ->
  i = 1
  $(box).find('input.add_passager').click ->
    code = '<tr><td class="fio"><input type="text" name="train[passager][' + i + '][fio]" required></td><td><input type="text" name="train[passager][' + i + '][birth]" required></td></tr>'
    $(code).insertBefore $(box).find('tr.after_pass')
    i++
    false