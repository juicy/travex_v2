module CountriesHelper

  def visas
    @visa_link = true
    countries = Country.where("visa_information <> ''")
    render partial: 'layouts/elems', locals: {elems: countries}
  end

end
