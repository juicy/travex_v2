module ApplicationHelper

  def server
    'http://spec.gooddays.ru/.juicy/'
    #'http://travex-local-php/'
  end

  def hotel_mail_path
    "#{server}hotel.php"
  end

  def avia_mail_path
    "#{server}avia.php"
  end

  def train_mail_path
    "#{server}train.php"
  end

  def drop(file)
    url = URI.parse("https://dl.dropboxusercontent.com/u/4829990/")
    p = file.path
    p = p.match(/^Public\//).post_match
    url = "#{url}#{p}"
    url
  end

  def countries
    counrties = Country.all(order: 'title ASC')
    render partial: 'layouts/elems', locals: {elems: counrties}
  end

  def cities(country = nil)
    if country
      cities = country.cities
    else
      cities = City.all
    end
    
    render partial: 'layouts/elems', locals: {elems: cities}
  end

  def hotels(country = nil)
    if country
      hotels = country.hotels
    else
      hotels = Hotel.all
    end
    render partial: 'layouts/elems', locals: {elems: hotels}
  end

  def menu
    @menus = Menu.all(order: 'sort')
    render partial: 'layouts/menu'
  end

  def box(name)
    @boxs.each do |box|
      if box.name == name
        return raw(box.content)
      end
    end
  end

end