require 'test_helper'

class MailControllerTest < ActionController::TestCase
  test "should get avia" do
    get :avia
    assert_response :success
  end

  test "should get hotel" do
    get :hotel
    assert_response :success
  end

end
